# Extract a compressed file
function extract {
    if [ -z "$1" ]; then
# display usage if no parameters given
        echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
        echo "       extract <path/file_name_1.ext> [path/file_name_2.ext] [path/file_name_3.ext]"
    else
        for n in $@
            do
                if [ -f "$n" ] ; then
                    case "${n%,}" in
                        *.tar.bz2|*.tar.gz|*.tar.xz|*.tbz2|*.tgz|*.txz|*.tar)
                            tar xvf "$n"       ;;
                        *.lzma)      unlzma ./"$n"      ;;
                        *.bz2)       bunzip2 ./"$n"     ;;
                        *.rar)       unrar x -ad ./"$n" ;;
                        *.gz)        gunzip ./"$n"      ;;
                        *.zip)       unzip ./"$n"       ;;
                        *.z)         uncompress ./"$n"  ;;
                        *.7z|*.arj|*.cab|*.chm|*.deb|*.dmg|*.iso|*.lzh|*.msi|*.rpm|*.udf|*.wim|*.xar)
                            7z x ./"$n"        ;;
                        *.xz)        unxz ./"$n"        ;;
                        *.exe)       cabextract ./"$n"  ;;
                        *)
                            echo "extract: '$n' - unknown archive method"
                            return 1
                            ;;
                    esac
                else
                    echo "'$n' - file does not exist"
                    return 1
                fi
            done
    fi
}

# Find the size of a directory
function size {
    if [ -z "$1" ]; then
# Do the current directory
        du -Sh -d 1 .
    else
        if [ -d "$1" ]; then
            du -Sh -d 1 $1
        else
            echo "'$2' Not a valid directory"
                return 1
        fi
    fi
}

# Create a new directory and enter it
function mkd() {
    mkdir -p "$@" && cd "$_";
}

# Create a .tar.gz archive, using `zopfli`, `pigz` or `gzip` for compression
function targz() {
    local tmpFile="${@%/}.tar";
    tar -cvf "${tmpFile}" --exclude=".DS_Store" "${@}" || return 1;

    size=$(
            stat -f"%z" "${tmpFile}" 2> /dev/null; # macOS `stat`
            stat -c"%s" "${tmpFile}" 2> /dev/null;  # GNU `stat`
          );

    local cmd="";
    if (( size < 52428800 )) && hash zopfli 2> /dev/null; then
# the .tar file is smaller than 50 MB and Zopfli is available; use it
        cmd="zopfli";
    else
        if hash pigz 2> /dev/null; then
            cmd="pigz";
        else
            cmd="gzip";
        fi
    fi

    echo "Compressing .tar ($((size / 1000)) kB) using \`${cmd}\`…";
    "${cmd}" -v "${tmpFile}" || return 1;
    [ -f "${tmpFile}" ] && rm "${tmpFile}";

    zippedSize=$(
            stat -f"%z" "${tmpFile}.gz" 2> /dev/null; # macOS `stat`
            stat -c"%s" "${tmpFile}.gz" 2> /dev/null; # GNU `stat`
            );

    echo "${tmpFile}.gz ($((zippedSize / 1000)) kB) created successfully.";
}


function gwhere {
    if [ -z "$1" ]; then
        # Do the current directory
        if [ -d .git ]; then
            awk '/url/ { print $3 }' .git/config
        fi
    else
        if [ -d "$1" ] && [ -f $1/.git/config ]; then
            awk '/url/ { print $3 }' $1/.git/config
        else
            echo "'$1' Not a valid directory for gwhere"
                return 1
        fi
    fi
}

function e {
    if [ -z "$1" ]; then
        # Do the current directory
        ez $1
    else
        vim $1
    fi
}

ez() {
  files=()
  while IFS= read -r -d '' file; do
    file=$(echo $file | sed "s/\//\/\//g")
    files+=("$file")
  done < <(fzf --multi --print0)

  (( ${#files} )) || return
  "${VISUAL:-${EDITOR:-vi}}" "$@" "${files[@]}"
}
