# Aliases
alias vi=vim
alias :EC="vim ~/.vimrc"
alias EC="vim ~/.vimrc"

alias gbr="git branch -a"
alias gs="git status"

alias sl=ls
alias claer=clear

alias update="sudo apt-get update && sudo apt-get upgrade -y"
alias hist=history
alias g="vim --remote-silent"

if hash neofetch 2>/dev/null; then
    alias version=neofetch
else
    alias version="lsb_release -a && uname -r"
fi

# Easier navigation: .., ..., ...., ....., ~ and -
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."

# USE THE COLORS
colorflag="--color"

# List all files colorized in long format
alias l="ls -lF ${colorflag}"

# List all files colorized in long format, including dot files
alias la="ls -laF ${colorflag}"

# List only directories
alias lsd="ls -lF ${colorflag} | grep --color=never '^d'"

# Always use color output for `ls`
alias ls="command ls ${colorflag}"

# Always enable colored `grep` output
# Note: `GREP_OPTIONS="--color=auto"` is deprecated, hence the alias usage.
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# Enable aliases to be sudo’ed
alias sudo='sudo '

# IP addresses
alias ip="dig +short myip.opendns.com @resolver1.opendns.com"
alias localip="ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'"
alias ips="ifconfig -a | grep -o 'inet6\? \(addr:\)\?\s\?\(\(\([0-9]\+\.\)\{3\}[0-9]\+\)\|[a-fA-F0-9:]\+\)' | awk '{ sub(/inet6? (addr:)? ?/, \"\"); print }'"

# Show active network interfaces
alias ifactive="ifconfig | pcregrep -M -o '^[^\t:]+:([^\n]|\n\t)*status: active'"

# View HTTP traffic
alias sniff="sudo ngrep -d 'en1' -t '^(GET|POST) ' 'tcp and port 80'"
alias httpdump="sudo tcpdump -i en1 -n -s 0 -w - | grep -a -o -E \"Host\: .*|GET \/.*\""

# Print each PATH entry on a separate line
alias path='echo -e ${PATH//:/\\n}'

# Let colors work in tmux
alias tmux="tmux -2"

# Load a tmux session rapidly
alias tmux0="tmux a -t 0"

# Clean the workspace
alias cleanup="rm -rf scripts ; rm -rf rundata ; rm *.out"
