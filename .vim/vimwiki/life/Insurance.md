# Allstate Meeting Notes 2019-01-14
Attend: KE, Jan Nielsen

## Notes
* Write a will immediately after Sylvia is pregnant
* Get Long-term care coverage at next opportunity for Open Enrollment.  This will rate-fix

### Home insurance
* 5x deductable before considering making a claim (generally)

### Life coverage
    * Need to have another meeting with Jan and Sylvia to have this discussion

# Action Items
* DONE Evaluate changes to auto policy including new rental car coverage (Updated for rental insurance)
* DONE Water/Sewer backup coverage - currently have 5k.  Should pick 10 or 25k.  25k is 190 per year, 10 is 165 per year (25k)
* DONE Jewelry $10 per $1000 per year for coverage to any loss, no deductable. (Sylvia Declines 2019/1/15)
