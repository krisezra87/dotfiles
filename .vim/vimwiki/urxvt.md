# URXVT

Perl extensions available from
> pacman -S urxvt-perls

Resize font on the fly
> yay -S urxvt-resize-font-git
