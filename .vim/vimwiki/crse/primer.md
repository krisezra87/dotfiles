# PPP Primer

What we're expecting is a BRIEF report -- 1 page, (okay, a little spill-over
into a second page is fine); keep it simple and straightforward.

__Progress__
In this section describe what accomplishments you've made in the
reporting period on your projects.  This can be mundane things (like attended
CCS, worked on getting acquainted with Azure, etc.) but also very direct
milestone progress on your individual projects.  These need not be 'outcomes'
but completing keys steps, etc.


__Plans__
In this section, talk about what you intend to do in the coming
month.  What activities do you envision you'll do?  Again, this can be mundane
things, but more and more we'd like to hear what you're doing with your work.

__Problems__
In this section, tell us what problems (challenges, concerns)
you're encountering.  Define these as things local to your environments (i.e.,
things that PTI may not be able to influence), but especially things related to
the CRSE program (i.e., things that PTI CAN influence).  An example  of the
latter might be:  Need access to Azure credits!  (hint-hint).  What we want to
do is understand what roadblocks you may be encountering in completing your
efforts on the schedule of your tenure with the program.  As a bit of
definition -- 'challenges' are things that are arising, but for which you have
an ability to overcome; 'concerns' are challenges that you fear could impede
your progress.  Again, while we're especially wanting to know about things we
can help with, it's important for you to alert us to things beyond "our"
control, just so we know.  Perhaps we can give you some counsel on those?
We'll see ...

Send these PPP Reports to me.  I'd like to see them no later than the close of
business (COB) on the second business day after the previous month closes.  For
example, February ends on a Thursday (2/28); your report is due to me COB on
Monday March 4.

Once again, this is meant to be a lightweight task -- so don't write me a book!
