Kris Ezra PPP January/February 2019

Progress:

In the last two months, I have made beginning progress toward the completion of my CRSE project related to the writing of a paper on the topic of the benefits of the Discrete Agent Framework (DAF) as enabled by cloud computing resources from Azure.  I attended the Microsoft Campus Connections Summit (CCS) in Seattle to present an overview of my work and begin to understand the enabling features of Azure.  Based on insights gained from CCS, I was able to create a personal account with Azure and begin a preliminary exploration of its offerings.  Specifically, I created a functioning virtual machine and began to explore batch processing capabilities.

I have begun a literature review in my topic area and started to identify key papers and historical references to other agent-based modeling approaches as well as to collect seminal papers related to agent-based modeling to enable system-of-systems (SoS) research at Purdue.  This process is ongoing in tandem with further exploration of Azure.

Plans:

In the coming month, I plan to continue my literature review and create an enterprise account which will let me leverage computational credits from Microsoft.  While continuing to explore Azure, I plan to download and install Matlab to a virtual machine.  This will also require identifying the best methods to connect to Purdue's Matlab license server which should be possible using openvpn on a virtual machine.  With Matlab installed, I can begin preliminary development of my test environment which will serve as a demonstration of interest for my paper.  As possible, I will identify needs for interfacing with batch processing infrastructure available from Azure.  I will continue to identify papers and resources of interest related to other agent-based modeling tools and foundational research in the area of SoS modeling and simulation.

Problems:

Depending on forward progress with Azure, I expect to need access to Azure compute credits.  My primary forseeable challenge will be to get an Azure-deployed virtual machine to drive Azure batch-processing infrastructure.  Given a full travel schedule for the end of February and mid-March, I will also need to develop a roadmap to ensure continued and satisfactory forward progress is being made toward paper progress.  Additionally, I have identified a potential conflict with the delivery of a project overview in Chicago in August as this overlaps with a Hypersonics conference at Purdue University I may be required to attend.  As both agendas are released, I will work toward attending both since they are both within reasonable driving distance.
