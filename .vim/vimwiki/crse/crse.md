## CRSE Notes

[2019-04-12](2019-04-12.md) - Craig Template\
[2019 01 23](2019-01-23) - Kickoff call with Brian Voss\
[2019 01 31](2019-01-31) - Assignments\
[2019 02 07](2019-02-07) - Campus Connections Summit (CCS) @ Microsoft

[paper_summary](paper_summary.md) - Short description of proposed paper

## PPP - Progress, Plans, Problems

[primer](primer.md) - From Brian
[PPP_jan_feb](PPP_jan_feb.md) - PPP for January and February
[PPP_march](PPP_march.md) - PPP for March
paper_guidelines - from PEARC19 call
