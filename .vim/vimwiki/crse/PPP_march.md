Kris Ezra PPP March 2019

Progress:

In the last month, I have made continued progress toward the completion of my CRSE project related to writing a paper on the benefits of the Discrete Agent Framework (DAF) as enabled by Azure.  I am continuing my literature review and have tabulated submission requirements for my targeted journal.  The majority of my efforts this month have been focused on developing a working version of my small UAV tracking simulator as a proof of concept for my methodology.  Some initial work has gone into the spawning of UAV swarms, but a key push has been in the usage of Matlab as a headless linux server as part of my planned steps toward tradespace exploration in Azure using Platform as a Service (PaaS).  To further my work in Azure, I have created my Indiana University user account and am awaiting further instruction to continue exploring the capabilities of Azure to support my paper.

Plans:

In the coming month, I plan to continue my literature review, begin writing the outline and framework of my paper, and begin work in the Azure environment on a virtual machine to pilot my simulation as it exists.  Using the information I have gained regarding Purdue Univerity's enterprise Matlab license server, I believe I can create a functional proof-of-concept as soon as my Azure credits arrive.  When this simulator is deployed as a pilot, I will continue my investigation of batch processing techniques using the PaaS capabilities offered by Azure. As possible, I will identify needs for interfacing with batch processing infrastructure available from Azure.  I will continue to identify papers and resources of interest related to other agent-based modeling tools and foundational research in the area of SoS modeling and simulation.

After some planning, I will be meeting with Craig at the end of next week to complete my project template.  I will review his template in the coming week to ensure that this one on one call goes as seamlessly as possible.

Problems:

This month, I spent approximately three weeks on travel and vacation which has greatly reduced my ability to test Azure capabilities, though I have been able to continue my literature review.  On last login, I found that my free trial has expired for some reason and as such I'm unable to continue this exploration until our Azure credits arrive.  I think that, rather than creating an additional free account using my second personal email address, it makes more sense to wait until these credits arrive to avoid future disruptions and rehashing completed work.  No further information about my future conflict with PEARC in August has been forthcoming as it's still too early to know the schedule of the Purdue Hypersonics Conference with enough certainty to make additional travel plans.  
