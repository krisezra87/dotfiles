#!/usr/bin/env bash

cd ~
mkdir python3
cd python3
wget https://www.python.org/ftp/python/3.7.1/Python-3.7.1.tgz
tar -xzf Python-3.7.1.tgz
cd Python-3.7.1/

./configure --prefix=$HOME/opt/python3
make && make install

mkdir -p $HOME/bin
cd $HOME/bin

ln -s $HOME/opt/python3/bin/python3

$HOME/bin/python3 setup.py install --prefix=$HOME/opt/python3

which python3
