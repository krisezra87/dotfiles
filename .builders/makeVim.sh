#!/usr/bin/env bash

# Modified from https://github.com/Valloric/YouCompleteMe/wiki/Building-Vim-from-source

# For package installations if necessary
# sudo apt install libncurses5-dev libgnome2-dev libgnomeui-dev \
# libgtk2.0-dev libatk1.0-dev libbonoboui2-dev \
#     libcairo2-dev libx11-dev libxpm-dev libxt-dev python-dev \
#     python3-dev ruby-dev lua5.1 liblua5.1-dev libperl-dev git

sudo apt remove vim vim-runtime gvim

cd ~
git clone https://github.com/vim/vim.git

cd vim

./configure --with-features=huge --enable-multibyte --enable-python3interp=yes --with-python3-config-dir=/usr/lib/python3.5/config-3.5m-x86_64-linux-gnu --enable-cscope --prefix=/usr/local

make VIMRUNTIMEDIR=/usr/local/share/vim/vim81

cd ~/vim
sudo make install

# Update default editor information if not already done
sudo update-alternatives --install /usr/bin/editor editor /usr/local/bin/vim 1
sudo update-alternatives --set editor /usr/local/bin/vim
sudo update-alternatives --install /usr/bin/vi vi /usr/local/bin/vim 1
sudo update-alternatives --set vi /usr/local/bin/vim

# Check the version information
vim --version
