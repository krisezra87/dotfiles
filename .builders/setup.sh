#bin/bash

git clone kezra@rice.rcac.purdue.edu:~/GIT/dotfileMirror dotfiles
rsync -a dotfiles/ ~/
rmdir dotfiles

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

vim +PluginInstall +qall

source ~/.bashrc
