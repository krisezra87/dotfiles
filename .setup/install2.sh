#!/bin/bash

# Install the stuff from the regular repositories
sudo pacman -S chromium dmenu feh fontforge gdm git gnome gnome-tweak-tool gvim i3-wm libreoffice-fresh lynx mutt neomutt openconnect openssh powerline-fonts pulseaudio ranger tmux rxvt-unicode urxvt-perls xf86-video-amdgpu xf86-video-vesa xorg xorg-apps xorg-server xorg-xinit zathura-pdf-poppler zathura-ps networkmanager bat exa xterm

# Get the stuff we want from AUR
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
cd ..
rm -rf yay

yay -S chromium-widevine urxvt-resize-font-git
yay -S libmutter2 &

# Install my configurations
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall

sudo systemctl enable NetworkManager.service
sudo systemctl enable sshd.service
sudo systemctl restart sshd.service
